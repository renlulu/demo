package com.labs.demo.controller;

import com.labs.demo.controller.vo.ProductVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {


    @PostMapping
    @ApiOperation(value = "增加商品", notes = "")
    public ResponseEntity add(@RequestBody ProductVo productVo) {

        return ResponseEntity.ok().build();
    }

    @GetMapping
    @ApiOperation(value = "查看所有商品", notes = "")
    public ResponseEntity all() {
        return ResponseEntity.ok().build();
    }
}
