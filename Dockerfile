FROM java

LABEL maintainer "xiaohuo.ren@daocloud.io"

WORKDIR /usr/src/myapp/

COPY build/libs/demo-0.0.1-SNAPSHOT.jar /usr/src/myapp/demo-0.01-SNAPSHOT.jar

RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo "Asia/Shanghai" > /etc/timezone

WORKDIR /usr/src/myapp/

ENTRYPOINT java -jar demo-0.01-SNAPSHOT.jar